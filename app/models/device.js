import Model, { attr } from '@ember-data/model';

export default class DeviceModel extends Model {
  @attr name;
  @attr mac;
}
