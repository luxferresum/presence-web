import ApplicationAdapter from './application';

export default class DeviceAdapter extends ApplicationAdapter {
  urlForQueryRecord(query) {
    if (query.current) {
      delete query.current;
      return '/api/device';
    }

    throw 'unknown query for device';
  }
}
