import RESTAdapter from '@ember-data/adapter/rest';
import { pluralize } from 'ember-inflector';
import { inject as service } from '@ember/service';

function serializeIntoHash(
  store,
  modelClass,
  snapshot,
  options = { includeId: true }
) {
  const serializer = store.serializerFor(modelClass.modelName);

  if (typeof serializer.serializeIntoHash === 'function') {
    const data = {};
    serializer.serializeIntoHash(data, modelClass, snapshot, options);
    return data;
  }

  return serializer.serialize(snapshot, options);
}

export default class ApplicationAdapter extends RESTAdapter {
  @service session;

  get headers() {
    return {
      Authorization: `Bearer ${this.session.data.authenticated.token}`,
    };
  }

  urlForFindAll(modelName) {
    return `/api/${pluralize(modelName)}`;
  }

  urlForCreateRecord(modelName) {
    return `/api/${modelName}`;
  }

  urlForDeleteRecord(id, modelName) {
    return `/api/${modelName}/${id}`;
  }

  createRecord(store, type, snapshot) {
    console.log('bla');
    let url = this.buildURL(type.modelName, null, snapshot, 'createRecord');
    const data = serializeIntoHash(store, type, snapshot);

    return this.ajax(url, 'PUT', { data });
  }

  handleResponse(status) {
    if (status === 401 && this.session.isAuthenticated) {
      this.session.invalidate();
    }
    return super.handleResponse(...arguments);
  }
}
