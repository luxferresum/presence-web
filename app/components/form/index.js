import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class FormComponent extends Component {
  @action
  submit(event) {
    event.preventDefault();
    this.args.submit?.(...arguments);
  }
}
