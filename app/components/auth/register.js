import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class AuthRegisterComponent extends Component {
  @service session;

  @tracked username;
  @tracked password;
  @tracked email;
  @tracked anonymous = false;

  @action
  async submit() {
    await fetch('/api/user', {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: this.username,
        password: this.password,
        email: this.email,
        anonymous: this.anonymous,
      }),
    });

    await this.session.authenticate('authenticator:presence', {
      username: this.username,
      password: this.password,
    });
  }
}
