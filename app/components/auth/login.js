import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class AuthLoginComponent extends Component {
  @service session;
  @service notifications;

  @tracked username;
  @tracked password;

  @action
  async submit() {
    try {
      await this.session.authenticate('authenticator:presence', {
        username: this.username,
        password: this.password,
      });
    } catch (err) {
      if (!err.errors) {
        this.notifications.error('something strange happened? lol');
      }
      for (const error of err.errors) {
        this.notifications.error(error.detail ?? error.title, {
          autoClear: true,
        });
      }
    }
  }
}
