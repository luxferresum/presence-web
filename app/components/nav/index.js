import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class NavComponent extends Component {
  @service session;

  @action
  invalidate() {
    this.session.invalidate();
  }
}
