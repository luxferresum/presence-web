import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class DevicesComponent extends Component {
  @service store;
  @service notifications;

  @tracked currentDevice = null;
  @tracked loadingDone = false;
  @tracked isInLocalNetwork = false;
  @tracked newDeviceName = 'New Device';

  constructor() {
    super(...arguments);
    this.load();
  }

  get canRegisterCurrentDevice() {
    return (
      this.loadingDone && this.isInLocalNetwork && this.currentDevice == null
    );
  }

  get showNotInLocalNetworkMsg() {
    return this.loadingDone && !this.isInLocalNetwork;
  }

  get devices() {
    return this.args.devices.filter(d => !d.isNew);
  }

  async load() {
    try {
      this.currentDevice = await this.store.queryRecord('device', {
        current: true,
      });
      this.loadingDone = true;
      this.isInLocalNetwork = true;
    } catch (err) {
      this.loadingDone = true;
      this.isInLocalNetwork = true;
      if (err.errors.some((x) => x.code === 'DeviceNotRegistered')) {
        return;
      }

      if (err.errors.some((x) => x.code === 'NotOnLocalNetwork')) {
        this.isInLocalNetwork = false;
        return;
      }

      console.error(err);
      throw err;
    }
  }

  @action
  async addCurrentDevice() {
    this.currentDevice = this.store.createRecord('device', {
      name: this.newDeviceName,
    });
    try {
      await this.currentDevice.save();
    } catch ({ errors }) {
      for (const error of errors) {
        this.notifications.error(error.detail ?? error.title, {
          autoClear: true,
        });
      }
      this.currentDevice.destroyRecord();
      this.currentDevice = null;
    }
  }

  @action
  async delete(device) {
    if (device.id === this.currentDevice?.id) {
      this.currentDevice = null;
    }
    try {
      await device.destroyRecord();
    } catch({ errors }) {
      for (const error of errors) {
        this.notifications.error(error.detail ?? error.title, {
          autoClear: true,
        });
      }
    }
  }
}
