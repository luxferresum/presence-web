import Base from 'ember-simple-auth/authenticators/base';

export default Base.extend({
  async restore(data) {
    const req = await fetch('/api/refresh_token', {
      headers: {
        Authorization: `Bearer ${data.token}`,
      },
    });

    const result = await req.json();
    if (result?.error) {
      throw result.error;
    }

    return {
      ...data,
      ...result,
    };
  },

  async authenticate({ username, password }) {
    const req = await fetch('/api/login', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ username, password }),
    });

    if (req.status < 200 || req.status >= 300) {
      throw await req.json();
    }

    return {
      username,
      ...(await req.json()),
    };
  },

  invalidate() {
    return Promise.resolve(null);
  },
});
