'use strict';

module.exports = {
  extends: 'recommended',
  ignore: {
    Form: ['submit'],
  },
};
